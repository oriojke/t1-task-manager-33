package ru.t1.didyk.taskmanager.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.model.Project;

@Getter
@Setter
@NoArgsConstructor
public class ProjectCompleteByIdResponse extends AbstractProjectResponse {

    public ProjectCompleteByIdResponse(@Nullable Project project) {
        super(project);
    }
}

package ru.t1.didyk.taskmanager.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public class UserUnlockRequset extends AbstractUserRequest {

    @Nullable
    private String login;

    public UserUnlockRequset(@Nullable String token) {
        super(token);
    }
}

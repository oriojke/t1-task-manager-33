package ru.t1.didyk.taskmanager.api.service;

import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.api.repository.IRepository;
import ru.t1.didyk.taskmanager.enumerated.Sort;
import ru.t1.didyk.taskmanager.model.AbstractModel;

import java.util.List;

public interface IService<M extends AbstractModel> extends IRepository<M> {

    @Nullable
    List<M> findAll(@Nullable Sort sort);

}

package ru.t1.didyk.taskmanager.api.service;

import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.api.repository.IUserOwnedRepository;
import ru.t1.didyk.taskmanager.enumerated.Sort;
import ru.t1.didyk.taskmanager.model.AbstractUserOwnedModel;

import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M> {

    @Nullable
    List<M> findAll(@Nullable String userId, @Nullable Sort sort);

    @Nullable
    List<M> findAll();

}

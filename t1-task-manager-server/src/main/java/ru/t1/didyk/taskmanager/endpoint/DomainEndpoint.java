package ru.t1.didyk.taskmanager.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.didyk.taskmanager.api.endpoint.IDomainEndpoint;
import ru.t1.didyk.taskmanager.api.service.IServiceLocator;
import ru.t1.didyk.taskmanager.dto.request.*;
import ru.t1.didyk.taskmanager.dto.response.*;
import ru.t1.didyk.taskmanager.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.didyk.taskmanager.api.endpoint.IDomainEndpoint")
public class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {

    public DomainEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }


    @Override
    @WebMethod
    public DataBackupLoadResponse domainBackupLoad(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBackupLoadRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataBackupLoad();
        return new DataBackupLoadResponse();
    }

    @Override
    @NotNull
    @WebMethod
    public DataBackupSaveResponse domainBackupSave(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBackupSaveRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataBackupSave();
        return new DataBackupSaveResponse();
    }

    @Override
    @NotNull
    @WebMethod
    public DataBase64LoadResponse domainBase64Load(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBase64LoadRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataBase64Load();
        return new DataBase64LoadResponse();
    }

    @Override
    @NotNull
    @WebMethod
    public DataBase64SaveResponse domainBase64Save(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBase64SaveRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataBase64Save();
        return new DataBase64SaveResponse();
    }

    @Override
    @NotNull
    @WebMethod
    public DataBinaryLoadResponse domainBinaryLoad(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBinaryLoadRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataBinaryLoad();
        return new DataBinaryLoadResponse();
    }

    @Override
    @NotNull
    @WebMethod
    public DataBinarySaveResponse domainBinarySave(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBinarySaveRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataBinarySave();
        return new DataBinarySaveResponse();
    }

    @Override
    @NotNull
    @WebMethod
    public DataJsonLoadFasterXmlResponse domainJsonLoadFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataJsonLoadFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataJsonLoadFasterXml();
        return new DataJsonLoadFasterXmlResponse();
    }

    @Override
    @NotNull
    @WebMethod
    public DataJsonLoadJaxBResponse domainJsonLoadJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataJsonLoadJaxBRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataJsonLoadJaxB();
        return new DataJsonLoadJaxBResponse();
    }

    @Override
    @NotNull
    @WebMethod
    public DataJsonSaveFasterXmlResponse domainJsonSaveFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataJsonSaveFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataJsonSaveFasterXml();
        return new DataJsonSaveFasterXmlResponse();
    }

    @Override
    @NotNull
    @WebMethod
    public DataJsonSaveJaxBResponse domainJsonSaveJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataJsonSaveJaxBRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataJsonSaveJaxB();
        return new DataJsonSaveJaxBResponse();
    }

    @Override
    @NotNull
    @WebMethod
    public DataXmlLoadFasterXmlResponse domainXmlLoadFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataXmlLoadFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataXmlLoadFasterXml();
        return new DataXmlLoadFasterXmlResponse();
    }

    @Override
    @NotNull
    @WebMethod
    public DataXmlLoadJaxBResponse domainXmlLoadJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataXmlLoadJaxBRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataXmlLoadJaxB();
        return new DataXmlLoadJaxBResponse();
    }

    @Override
    @NotNull
    @WebMethod
    public DataXmlSaveFasterXmlResponse domainXmlSaveFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataXmlSaveFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataXmlSaveFasterXml();
        return new DataXmlSaveFasterXmlResponse();
    }

    @Override
    @NotNull
    @WebMethod
    public DataXmlSaveJaxBResponse domainXmlSaveJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataXmlSaveJaxBRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataXmlSaveJaxB();
        return new DataXmlSaveJaxBResponse();
    }

    @Override
    @NotNull
    @WebMethod
    public DataYamlLoadFasterXmlResponse domainYamlLoadFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataYamlLoadFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataYamlLoadFasterXml();
        return new DataYamlLoadFasterXmlResponse();
    }

    @Override
    @NotNull
    @WebMethod
    public DataYamlSaveFasterXmlResponse domainYamlSaveFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataYamlSaveFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataYamlLoadFasterXml();
        return new DataYamlSaveFasterXmlResponse();
    }
}

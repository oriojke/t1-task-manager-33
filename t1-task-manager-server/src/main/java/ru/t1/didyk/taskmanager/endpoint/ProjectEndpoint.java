package ru.t1.didyk.taskmanager.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.api.endpoint.IProjectEndpoint;
import ru.t1.didyk.taskmanager.api.service.IProjectService;
import ru.t1.didyk.taskmanager.api.service.IServiceLocator;
import ru.t1.didyk.taskmanager.dto.request.*;
import ru.t1.didyk.taskmanager.dto.response.*;
import ru.t1.didyk.taskmanager.enumerated.Sort;
import ru.t1.didyk.taskmanager.enumerated.Status;
import ru.t1.didyk.taskmanager.model.Project;
import ru.t1.didyk.taskmanager.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "ru.t1.didyk.taskmanager.api.endpoint.IProjectEndpoint")
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    public IProjectService getProjectService() {
        return getServiceLocator().getProjectService();
    }

    @Override
    @NotNull
    @WebMethod
    public ProjectChangeStatusByIdResponse changeStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectChangeStatusByIdRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getId();
        @Nullable final Status status = request.getStatus();
        @Nullable final Project project = getProjectService().changeProjectStatusById(userId, projectId, status);
        return new ProjectChangeStatusByIdResponse(project);
    }

    @Override
    @NotNull
    @WebMethod
    public ProjectChangeStatusByIndexResponse changeStatusByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectChangeStatusByIndexRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final Status status = request.getStatus();
        @Nullable final Project project = getProjectService().changeProjectStatusByIndex(userId, index, status);
        return new ProjectChangeStatusByIndexResponse(project);
    }

    @Override
    @NotNull
    @WebMethod
    public ProjectClearResponse clearProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectClearRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        getProjectService().clear(userId);
        return new ProjectClearResponse();
    }

    @Override
    @NotNull
    @WebMethod
    public ProjectCompleteByIdResponse completeById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectCompleteByIdRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getId();
        @Nullable final Project project = getProjectService().changeProjectStatusById(userId, projectId, Status.COMPLETED);
        return new ProjectCompleteByIdResponse(project);
    }

    @Override
    @NotNull
    @WebMethod
    public ProjectCompleteByIndexResponse completeByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectCompleteByIndexRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final Project project = getProjectService().changeProjectStatusByIndex(userId, index, Status.COMPLETED);
        return new ProjectCompleteByIndexResponse(project);
    }

    @Override
    @NotNull
    @WebMethod
    public ProjectCreateResponse create(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectCreateRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final Project project = getProjectService().create(userId, name, description);
        return new ProjectCreateResponse(project);
    }

    @Override
    @NotNull
    @WebMethod
    public ProjectListResponse listProjects(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectListRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Sort sort = request.getSortType();
        @Nullable final List<Project> projects = getProjectService().findAll(userId, sort);
        System.out.println(userId);
        return new ProjectListResponse(projects);
    }

    @Override
    @NotNull
    @WebMethod
    public ProjectRemoveByIdResponse removeById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectRemoveByIdRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getId();
        @Nullable final Project project = getProjectService().findOneById(userId, projectId);
        getProjectService().removeById(userId, projectId);
        return new ProjectRemoveByIdResponse(project);
    }

    @Override
    @NotNull
    @WebMethod
    public ProjectRemoveByIndexResponse removeByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectRemoveByIndexRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final Project project = getProjectService().findOneByIndex(userId, index);
        getProjectService().removeByIndex(userId, index);
        return new ProjectRemoveByIndexResponse(project);
    }

    @Override
    @NotNull
    @WebMethod
    public ProjectShowByIdResponse showById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectShowByIdRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getId();
        @Nullable final Project project = getProjectService().findOneById(userId, projectId);
        return new ProjectShowByIdResponse(project);
    }

    @Override
    @NotNull
    @WebMethod
    public ProjectShowByIndexResponse showByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectShowByIndexRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final Project project = getProjectService().findOneByIndex(userId, index);
        return new ProjectShowByIndexResponse(project);
    }

    @Override
    @NotNull
    @WebMethod
    public ProjectStartByIdResponse startById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectStartByIdRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getId();
        @Nullable final Project project = getProjectService().changeProjectStatusById(userId, projectId, Status.IN_PROGRESS);
        return new ProjectStartByIdResponse(project);
    }

    @Override
    @NotNull
    @WebMethod
    public ProjectStartByIndexResponse startByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectStartByIndexRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final Project project = getProjectService().changeProjectStatusByIndex(userId, index, Status.IN_PROGRESS);
        return new ProjectStartByIndexResponse(project);
    }

    @Override
    @NotNull
    @WebMethod
    public ProjectUpdateByIdResponse updateById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectUpdateByIdRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final Project project = getProjectService().updateById(userId, projectId, name, description);
        return new ProjectUpdateByIdResponse(project);
    }

    @Override
    @NotNull
    @WebMethod
    public ProjectUpdateByIndexResponse updateByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectUpdateByIndexRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final Project project = getProjectService().updateByIndex(userId, index, name, description);
        return new ProjectUpdateByIndexResponse(project);
    }
}
